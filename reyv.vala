public class Application : Gtk.Window {
	public Application () {
		// Prepare Gtk.Window:
		this.title = "My Gtk.Image";
		this.window_position = Gtk.WindowPosition.CENTER;
		this.destroy.connect (Gtk.main_quit);
		this.set_default_size (350, 70);

		// BEGIN LOAD IMAGE FROM RESOURCE
		string path = "/reyv/resources/battery-icon.jpg";
		Gtk.Image image = new Gtk.Image.from_resource(path);
		this.add (image);
		// END LOAD IMAGE FROM RESOURCE
	}

	public static int main (string[] args) {
		Gtk.init (ref args);
		Application app = new Application ();
		app.show_all ();
		Gtk.main ();
		return 0;
	}
}
