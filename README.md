

DEPENDS
-------

This was developed under Debian Stretch during Stretch's testing phase.

As always with Linux, YYMV per distro.

I needed to install the following to get this to build and run:

libgtk-3-dev, make, gcc, libpng-dev 

CREDITS
-------

battery-icon.jpg is from here http://www.designer-daily.com/10-tutorials-to-learn-how-to-create-icons-with-photoshop-48742

Thanks Designer Daily

RESOURCES
---------

List of resources that I used to build this:

1. Gtk3 hello world

https://developer.gnome.org/gtk3/stable/gtk-getting-started.html

2. Pixdata info for the resource file was here:

http://wibblystuff.blogspot.com/2012/06/building-binary-for-gtk3-theme.html

<file preprocess="to-pixdata">assets/slider.png</file>

3. GResource

https://developer.gnome.org/gio/stable/GResource.html

to-pixdata which will use the gdk-pixbuf-pixdata command to convert images to
the GdkPixdata format, which allows you to create pixbufs directly using the
data inside the resource file, rather than an (uncompressed) copy if it. 

4. Vala GResource

https://gnulove.wordpress.com/category/vala/
