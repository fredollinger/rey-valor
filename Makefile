.PHONY: resources
TARGET=reyv
RESOURCE=$(TARGET)-resource.xml

# vala: resources.o hamburger.o
vala: resources.c
	valac --pkg gtk+-3.0 --pkg glib-2.0 resources.c reyv.vala -o reyv-v 

c: resources.c resources.o
	gcc `pkg-config --cflags gtk+-3.0` -o $(TARGET)-c $(TARGET).c `pkg-config --libs gtk+-3.0` resources.o

resources.o: resources.c
	gcc `pkg-config --cflags gtk+-3.0` -c resources.c `pkg-config --libs gtk+-3.0`

clean:
	rm -f $(TARGET) $(TARGET)-c $(TARGET)-v resources.h resources.c resources.o

resources.c:
	glib-compile-resources --target=resources.c --generate-source $(RESOURCE)

resources.h:
	glib-compile-resources --target=resources.h --generate-header $(RESOURCE)

edit:
	vim $(TARGET).vala

test:
	./$(TARGET)

reyv.gir:
	g-ir-scanner --verbose -n Reyv -I. --warn-all --namespace=Reyv hamburger.c hamburger.h `pkg-config --libs --cflags gtk+-3.0` -o reyv.gir

resources.gir:
	g-ir-scanner --verbose --warn-all -n Reyv -I. --namespace=Reyv resources.c resources.h `pkg-config --libs --cflags gio-2.0` -o reyv.gir

reyv.vapi:
	vapigen --pkg gtk+-3.0 --library reyv reyv.gir
